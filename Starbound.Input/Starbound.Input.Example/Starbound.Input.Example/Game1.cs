using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Starbound.Input.Example
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;

        private string message = "";

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // How to add mouse events.
            Components.Add(new MouseEvents(this));

            // How to add keyboard events.
            Components.Add(new KeyboardEvents(this));

            // How to add game pad events for all controllers. 
            // You can keep a reference to these GamePadEvents objects created here and remap the logical
            // player indices to new physical player indices, allowing you to have what you consider to be
            // "Player 1" in your game to use any game pad.
            Components.Add(new GamePadEvents(PlayerIndex.One, PlayerIndex.One, this));
            Components.Add(new GamePadEvents(PlayerIndex.Two, PlayerIndex.Two, this));
            Components.Add(new GamePadEvents(PlayerIndex.Three, PlayerIndex.Three, this));
            Components.Add(new GamePadEvents(PlayerIndex.Four, PlayerIndex.Four, this));

            // Examples of binding to various events. This uses lambda expressions for simplicity.
            // When you create your own methods that have some complexity to them, you'll probably want
            // to add them as full-fledged named methods and just list the name here.
            MouseEvents.ButtonPressed +=       (sender, e) => { Console.WriteLine(e.Button + " was pressed."); };
            MouseEvents.ButtonReleased +=      (sender, e) => { Console.WriteLine(e.Button + " was released."); };
            MouseEvents.ButtonClicked +=       (sender, e) => { Console.WriteLine(e.Button + " was clicked."); };
            MouseEvents.ButtonDoubleClicked += (sender, e) => { Console.WriteLine(e.Button + " was double clicked."); };
            MouseEvents.ButtonTripleClicked += (sender, e) => { Console.WriteLine(e.Button + " was triple clicked."); };
            MouseEvents.MouseMoved +=          (sender, e) => { Console.WriteLine(String.Format("The mouse moved from ({0}, {1}) to ({2}, {3})", e.Previous.X, e.Previous.Y, e.Current.X, e.Current.Y)); };
            MouseEvents.MouseDragged +=        (sender, e) => { Console.WriteLine(String.Format("The mouse was dragged from ({0}, {1}) to ({2}, {3})", e.Previous.X, e.Previous.Y, e.Current.X, e.Current.Y)); };
            MouseEvents.MouseWheelMoved += (sender, e) => { Console.WriteLine(String.Format("The mouse wheel was rotated by {0}.", e.Delta)); };
            KeyboardEvents.KeyPressed += (sender, e) => { Console.WriteLine(String.Format("Key Pressed: " + e.Key + " Modifiers: " + e.Modifiers)); };
            KeyboardEvents.KeyReleased += (sender, e) => { Console.WriteLine(String.Format("Key Released: " + e.Key + " Modifiers: " + e.Modifiers)); };
            KeyboardEvents.KeyTyped += (sender, e) => { Console.WriteLine(String.Format("Key Typed: " + e.Key + " Modifiers: " + e.Modifiers)); };
            KeyboardEvents.KeyTyped += KeyTyped;

            KeySequence.KonamiCode.KeySequenceEntered += (sender, e) => { message = "KONAMI!"; };
            KeyboardEvents.AddKeySequence(KeySequence.KonamiCode);

            GamePadEvents.ButtonDown += (sender, e) => { Console.WriteLine(e.Button + " was down for logical player " + e.LogicalIndex); };
            GamePadEvents.ButtonUp += (sender, e) => { Console.WriteLine(e.Button + " was up for logical player " + e.LogicalIndex); };
            GamePadEvents.LeftThumbstickMoved += (sender, e) => { Console.WriteLine("Left thumbstick moved."); };
            GamePadEvents.RightThumbstickMoved += (sender, e) => { Console.WriteLine("Right thumbstick moved."); };
            GamePadEvents.ThumbstickMoved += (sender, e) => { Console.WriteLine("A thumbstick moved."); };
            GamePadEvents.LeftTriggerMoved += (sender, e) => { Console.WriteLine("Left Trigger moved."); };
            GamePadEvents.RightTriggerMoved += (sender, e) => { Console.WriteLine("Right Trigger moved."); };
            GamePadEvents.TriggerMoved += (sender, e) => { Console.WriteLine("A Trigger moved."); };
            GamePadEvents.Connected += (sender, e) => { Console.WriteLine("Logical Player " + e.LogicalIndex + " connected."); };
            GamePadEvents.Disconnected += (sender, e) => { Console.WriteLine("Logical Player " + e.LogicalIndex + " disconnected."); };

            base.Initialize();
        }

        private void KeyTyped(object sender, KeyboardEventArgs e)
        {
            if(e.Character.HasValue) { message += e.Character.Value; }
            if (e.Key == Keys.Back && message.Length > 0) { message = message.Substring(0, message.Length - 1); }
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            font = Content.Load<SpriteFont>("Simple");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            //    this.Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();
            spriteBatch.DrawString(font, message, new Vector2(10, 10), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
